<?php
  require_once "../config.php";

  use \Tsugi\Core\LTIX;
  use \Tsugi\Util\U;

  $LTI = LTIX::requireData();
  $p = $CFG->dbprefix;

// Pulls all the data to display
  $names = $PDOX->allRowsDie("SELECT netId, imgUrl, name FROM {$p}sakai_attendance ORDER BY netId ASC");
  $notes = $PDOX->allRowsDie("SELECT * FROM {$p}notes_attendance ORDER BY date");
  $attend = $PDOX->allRowsDie("SELECT * FROM {$p}sakai_attendance");

  $OUTPUT->header();
?>
<link href="<?= U::get_rest_parent() ?>/main.css" rel="stylesheet" type="text/css"/>
<?php
  $OUTPUT->bodyStart();
  $OUTPUT->flashMessages();
?>
<h1>Class Summary</h1>
<a href="index.php" class="btn btn-default">Analyze Photos</a>
<a href="class.php" class="btn btn-default">Class Summary</a>
<a href="fullAttend.php" class="btn btn-default">Review Attendance</a>

<!-- Lists each student and their related data -->
<ul style="list-style-type:none">
  <?php foreach ($names as $name): ?>
    <li style="padding-top:10px;">
      <img src="<?php echo $name['imgUrl'] ?>" style="width:45%;height:auto;">
      <div style="display: inline-block;"class="information">
        <h3><?php echo($name['name'])?></h3>
        <h4><?php echo($name['netId'])?></h4>
        <?php foreach ($attend as $dates):
            if($dates['netId'] == $name['netId']){
              $present = 0;
              $missing = 0;
              foreach ($dates as $date):
                if($date == '1'){
                  $present++;
                } else if ($date == '0') {
                  $missing++;
                } else {
                  continue;
                }
              endforeach;
            }
          endforeach;
          echo("<b>Total times present:</b> $present<br>");
          echo("<b>Total times absent:</b> $missing");?>
        <!-- Display all the notes on a student and allows the professor to make more notes -->
        <h5>Notes</h5>
        <table>
        <?php foreach ($notes as $note):
          if($name['netId'] == $note['netId']){?>
            <tr>
              <td><b><?php echo(strftime('%B %e',strtotime($note['date'])))?></b></td>
              <td><?php echo $note['note']?></td>
            </tr>
          <?php }
        endforeach;?>
        </table>
        <form id="addNote<?php echo($name['netId'])?>" style="padding-top:5px;">
          <input type="submit" name="showNote" value="Add Note"
            onclick="$('#noteForm<?php echo($name['netId'])?>').toggle(); $('#addNote<?php echo($name['netId'])?>').toggle(); return false;"
            class="btn btn-default">
        </form>
        <form method="post" id="noteForm<?php echo($name['netId'])?>" style="display:none" action="addNote.php">
          <input type="hidden" value="<?php echo($name['netId']); ?>" name="netId">
          <textarea rows="5" cols="60" name="noteText"></textarea><br/>
          <input type="submit" name="submitNote" value="Submit note"  class="btn btn-success">
          <input type="submit" name="doCancel" onclick="$('#noteForm<?php echo($name['netId'])?>').toggle();  $('#addNote<?php echo($name['netId'])?>').toggle(); return false;" value="Cancel" class="btn btn-default">
        </form>
      </div>
    </li>
  <?php endforeach; ?>
</ul>


<?php
  $OUTPUT->footerStart();
  $OUTPUT->footerEnd();
?>
