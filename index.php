<?php
  header("Access-Control-Allow-Origin: *");
  header("Content-Type: application/json; charset=UTF-8");
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Max-Age: 3600");
  header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

  require_once "../config.php";

  use \Tsugi\Core\LTIX;

// Handles POST notifications and makes sure the sender is actually Amazon and not an outside party
  if ( isset($_SERVER['HTTP_X_AMZ_SNS_MESSAGE_TYPE'])){
    $amazonHeaderValue = $_SERVER['HTTP_X_AMZ_SNS_MESSAGE_TYPE'];
    $data = json_decode(file_get_contents('php://input'), true);
    if ( $data['SignatureVersion'] == '1'){
      $certUrl = $data['SigningCertURL'];
      $cert = file_get_contents($certUrl);
      $publickey = openssl_get_publickey($cert);
      // Handles the notication to subscribe to a certain topic
      if($amazonHeaderValue == 'SubscriptionConfirmation'){
        $text = "";
        $text .= "Message\n";
        $text .= $data['Message'] . "\n";
        $text .= "MessageId\n";
        $text .= $data['MessageId'] . "\n";
        $text .= "SubscribeURL\n";
        $text .= $data['SubscribeURL'] . "\n";
        $text .= "Timestamp\n";
        $text .= $data['Timestamp'] . "\n";
        $text .= "Token\n";
        $text .= $data['Token'] . "\n";
        $text .= "TopicArn\n";
        $text .= $data['TopicArn'] . "\n";
        $text .= "Type\n";
        $text .= $data['Type'] . "\n";
        $signature = base64_decode($data['Signature']);
        if ( openssl_verify($text, $signature, $publickey, OPENSSL_ALGO_SHA1) ){
          $subscribe = $data["SubscribeURL"];
          print_r($subscribe);
          $resp = file_get_contents($subscribe);
        } else {
          throw new Exception('Cannot validate, link not trusted');
        }
      // Parses notification information and decides how to change the databases accordingly
      } else if ($amazonHeaderValue == 'Notification'){
        $text = "";
        $text .= "Message\n";
        $text .= $data['Message'] . "\n";
        $text .= "MessageId\n";
        $text .= $data['MessageId'] . "\n";
        if ($data['Subject'] != "") {
          $text .= "Subject\n";
          $text .= $data['Subject'] . "\n";
        }
        $text .= "Timestamp\n";
        $text .= $data['Timestamp'] . "\n";
        $text .= "TopicArn\n";
        $text .= $data['TopicArn'] . "\n";
        $text .= "Type\n";
        $text .= $data['Type'] . "\n";
        $signature = base64_decode($data['Signature']);
        if ( openssl_verify($text, $signature, $publickey, OPENSSL_ALGO_SHA1) ){
          if ($data['Subject'] == 'Unknown'){
            $PDOX = LTIX::getConnection();
            $p = $CFG->dbprefix;
            $stmt = $PDOX->queryDie("INSERT INTO {$p}unknown_attendance
              (imgUrl)
              VALUES (:imgUrl)",
              array(
                ':imgUrl' => $data['Message']
              )
            );
          } else if ($data['Subject'] == 'Known'){
            $PDOX = LTIX::getConnection();
            $p = $CFG->dbprefix;
            $stmt = $PDOX->queryDie("UPDATE {$p}sakai_attendance
              SET isPresent = 1
              WHERE netId=:netId AND isPresent = 0",
              array(
                ':netId' => $data['Message']
              )
            );
          }
        } else {
          throw new Exception('Cannot validate, link not trusted');
        }
      } else {
        throw new Exception('Unknown message type');
      }
    } else {
      throw new Exception('Cannot validate signature version.');
    }
  // If there is a user, display the instructor view or send to "forbidden" link
  } else {
    $LTI = LTIX::requireData();
    if ( !$LTI->user->instructor){
      header("Location: ".addSession("forbidden.php"));
    } else {
      header("Location: ".addSession("instructor.php"));
    }
  }
?>
