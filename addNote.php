<?php
require_once "../config.php";

use \Tsugi\Core\LTIX;

$LAUNCH = LTIX::requireData();
$p = $CFG->dbprefix;

if ($USER->instructor) {
    $netId = $_POST["netId"];
    $noteText = $_POST["noteText"];
    $stmt = $PDOX->queryDie("INSERT INTO {$p}notes_attendance (netId, note)
      VALUES (:netId, :note)",
      array(
        ':netId' => $netId,
        ':note' => $noteText,
      )
    );
    header( 'Location: '.addSession('class.php') ) ;
}
