<?php

// The SQL to uninstall this tool
$DATABASE_UNINSTALL = array(
"drop table if exists {$CFG->dbprefix}unknown_attendance",
"drop table if exists {$CFG->dbprefix}sakai_attendance"
);

// The SQL to create the tables if they don't exist
$DATABASE_INSTALL = array(
array( "{$CFG->dbprefix}unknown_attendance",
  "create table {$CFG->dbprefix}unknown_attendance (
      imgUrl      VARCHAR(255) NOT NULL,
      timeIn      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

      UNIQUE(imgUrl)
      PRIMARY KEY (imgUrl)
  ) ENGINE = InnoDB DEFAULT CHARSET=utf8"),
array( "{$CFG->dbprefix}sakai_attendance",
  "create table {$CFG->dbprefix}sakai_attendance (
      netId       VARCHAR(255) NOT NULL,
      timeIn      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      isPresent   BOOLEAN NOT NULL DEFAULT FALSE

      UNIQUE(netId)
      PRIMARY KEY (netId)
  ) ENGINE = InnoDB DEFAULT CHARSET=utf8")
);
