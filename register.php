<?php

$REGISTER_LTI2 = array(
"name" => "Sakai Attendance Monitor",
"FontAwesome" => "fa-university",
"short_name" => "Attendance Tool",
"description" => "Uses MySQL and AWS services to record attendance in class.",
"messages" => array("launch", "launch_grade")
);
