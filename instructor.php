<?php
// Defining global variables
  define('ENDPOINT_REKOG', 'https://rekognition.us-east-1.amazonaws.com/');
  define('ENDPOINT_S3', 'https://s3.us-east-1.amazonaws.com');
  define('BUCKET', 'sakai-face-test');
  define('REGION', 'us-east-1');
  define('COLLECTION', 'test');
  require_once "../config.php";

  use \Tsugi\Core\LTIX;
  use \Tsugi\Util\U;
  use \Aws\Rekognition\RekognitionClient;
  use \Aws\S3\S3Client;

  $LTI = LTIX::requireData();
  $p = $CFG->dbprefix;

// Retrieving time data
  date_default_timezone_set('America/Indiana/Indianapolis');
  if(intval(strftime('%e')) % 2 == 0){
    $d = strftime('-%m/%e');
  } else {
    $date = new DateTime;
    $d = date_format(date_modify(new DateTime, '-1 day'), '-m/d');
  }

// Retrieving database data
  $names = $PDOX->allRowsDie("SELECT netId FROM {$p}sakai_attendance WHERE `isPresent{$d}`=0");
  $faces = $PDOX->allRowsDie("SELECT imgUrl, timeIn FROM {$p}unknown_attendance");
  $keys = $PDOX->rowDie("SELECT * FROM {$p}lti_key WHERE key_id=3");

// Handle your POST data here...
  if ( isset($_POST['student_id'])){
    // Creates AWS clients
    $rekognition = new Aws\Rekognition\RekognitionClient([
      'version' => '2016-06-27',
      'region'  => REGION,
      'endpoint'  => ENDPOINT_REKOG,
      'credentials' => array(
        'key' => $keys['key_key'],
        'secret'  => $keys['secret'],
      )
    ]);
    $s3 = new Aws\S3\S3Client([
      'version' => '2006-03-01',
      'region'  => REGION,
      'endpoint'  => ENDPOINT_S3,
      'credentials' => array(
        'key' => $keys['key_key'],
        'secret'  => $keys['secret'],
      )
    ]);
    foreach ($_POST['student_id'] as $key):
      $result = explode(',', $key);
      // Remove picture if the student is in or not in the class (professor, janitor, etc)
      if ($result[0] == 'delete'){
        $filename = explode('/', $result[1]);
        $stmt = $PDOX->queryDie("DELETE FROM {$p}unknown_attendance
          WHERE imgUrl=:imgUrl",
          array(
            ':imgUrl' => $result[1]
          )
        );
        $result = $s3->deleteObject(array(
          'Bucket'  => BUCKET,
          'Key' => "{$filename[4]}/{$filename[5]}",
        ));
      // Updates both databases to mark a student as present and retrains it
      } else {
        $filename = explode('/', $result[2]);
        $resp_rekog = $rekognition->IndexFaces(array(
          'CollectionId'  => COLLECTION,
          'ExternalImageId' => $result[0],
          'Image' => [
            'S3Object' => [
              'Bucket'  => BUCKET,
              'Name' => "{$filename[4]}/{$filename[5]}",
            ],
          ],
        ));
        $source = BUCKET.'/unknown/'.
        $resp_put = $s3->copyObject(array(
          'Bucket'  => BUCKET,
          'Key' => "detected/{$result[0]}/{$filename[5]}",
          'CopySource'  => urlencode(BUCKET."/unknown/{$filename[5]}"),
          'ACL' => 'public-read',
        ));
        $resp_del = $s3->deleteObject(array(
          'Bucket'  => BUCKET,
          'Key' => "{$filename[4]}/{$filename[5]}",
        ));
        $stmt = $PDOX->queryDie("UPDATE {$p}sakai_attendance
          SET `isPresent{$d}` = 1, `timeIn{$d}`=:timeIn, `imgUrl=:imgUrl`
          WHERE netId=:netId AND `isPresent{$d}` = 0",
          array(
            ':netId' => $result[0],
            ':timeIn' => $result[1],
            ':imgUrl' => "https://s3.amazonaws.com/".BUCKET."/detected/{$result[0]}/{$filename[5]}",
          )
        );
        $stmt = $PDOX->queryDie("DELETE FROM {$p}unknown_attendance
          WHERE imgUrl=:imgUrl",
          array(
            ':imgUrl' => $result[2]
          )
        );
      }
    endforeach;
    header('Location: '.addSession('index.php'));
    return;
  }
// Start of the output
  $OUTPUT->header();
?>
<link href="<?= U::get_rest_parent() ?>/main.css" rel="stylesheet" type="text/css"/>
<?php
  $OUTPUT->bodyStart();
  $OUTPUT->flashMessages();
?>
  <h1>Sakai Attendance Monitor</h1>
  <a href="index.php" class="btn btn-default">Analyze Photos</a>
  <a href="class.php" class="btn btn-default">Class Summary</a>
  <a href="fullAttend.php" class="btn btn-default">Review Attendance</a>
<?php
  if ((!empty($faces)) && (!empty($names))){
    echo("<p>Below are a list of faces that are unrecognisable to the machine learning algorthim, either because they are not in the class or because their face has been distorted.</p>");
    echo("<p>Beside each picture is a list of students that are not marked present in class. Select the student's net-id from the list and click <strong>Submit</strong> at the bottom of the page when you are finished.</p>");
    echo("<p>Please only mark each student once, and try to use the best picture if multiple of the same student are present.</p>");
    echo("<p>If the net-id that corresponds to the face is not present, then that student is already marked present and no further action must be done. Please select <span style=\"color:#f24e4e; font-weight:bold;\">Delete</span> at the bottom of the list.</p>");
?>

<!-- Displays all of the faces with a list of the unaccoutned for students for the professor to pick and mark as attended -->
<form method="post" action="instructor.php">
  <?php foreach ($faces as $face): ?>
    <div class "face-identify">
      <img src="<?php echo $face['imgUrl'] ?>">
      <div class="name-menu">
        <select name = "student_id[]">
          <option disabled selected>Select student (net-id)</option>
          <?php foreach ($names as $name): ?>
            <option value = "<?php echo $name['netId'] . ',' . $face['timeIn'] .',' . $face['imgUrl']?>"><?php echo $name['netId'] ?></option>
          <?php endforeach ?>
          <option value = "delete,<?php echo $face['imgUrl'] ?>">Delete</option>
        </select>
      </div>
    </div>
  <?php endforeach ?>
  <input type="submit" value="Submit">
</form>
<?php
  } else if ((!empty($names)) && (empty($faces))){
?>
    <h4>Not all of the students are present, but there are currently no photos to verify from.</h4>
    <h5>Absent students:</h5>
    <ul>
    <?php foreach ($names as $name) : ?>
      <li><?php echo $name['netId']?></li>
    <?php endforeach ?>
    </ul>
<?php
  } else {
    echo("<h3>All students accounted for and present</h3>");
  }
  $OUTPUT->footerStart();
  $OUTPUT->footerEnd();
?>
