<?php
  require_once "../config.php";

  use \Tsugi\Core\LTIX;
  use \Tsugi\Util\U;

  $LTI = LTIX::requireData();
  $p = $CFG->dbprefix;

// Pulls all the data to display
  $names = $PDOX->allRowsDie("SELECT netId, imgUrl, name FROM {$p}sakai_attendance ORDER BY netId ASC");
  $notes = $PDOX->allRowsDie("SELECT * FROM {$p}notes_attendance");
  $attend = $PDOX->allRowsDie("SELECT * FROM {$p}sakai_attendance");

  $OUTPUT->header();
?>
<link href="<?= U::get_rest_parent() ?>/main.css" rel="stylesheet" type="text/css"/>
<?php
  $OUTPUT->bodyStart();
  $OUTPUT->flashMessages();
?>
<h1>Class Attendance</h1>
<a href="index.php" class="btn btn-default">Analyze Photos</a>
<a href="class.php" class="btn btn-default">Class Summary</a>
<a href="fullAttend.php" class="btn btn-default">Review Attendance</a>

<table>
  <tr>
    <th>Student Name</th>
    <?php for ($i = 16;$i < 27;$i++){
      if(($i % 2) == 0){
        echo("<th>7/$i</th>");
      }
    }?>
    <th>Edit</th>
  </tr>
  <?php foreach ($attend as $name): ?>
    <tr>
      <th><?php echo $name['name']?> (<?php echo $name['netId'] ?>)</th>
      <!-- <form method="post" id=changeVal<?php //echo($name['netId'].$num++)?> style="display:none"> -->
      <?php $num = 0;
      foreach ($name as $date):
        if($date == '1'){
          echo("<th class=\"attendYes\">✓</th>");
        } else if ($date == '0'){
          echo("<th class=\"attendNo\">✗</th>");
        }?>
        <!-- <input type="checkbox" value="<?php //echo $date?>" name="decide"<?php //echo($name['netId'].$num++)?>> -->
        <?php}
      endforeach;?>
      <th>Edit Row</th>
    </tr>
  <?php endforeach;?>
</table>
